package com.wolfram.test_assignment.utilities;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

public class driverOptions {
	String ChromeURL = getClass().getClassLoader().getResource("chromedriver.exe").getPath();
	String FFURL = getClass().getClassLoader().getResource("geckodriver.exe").getPath();
	FirefoxOptions firefoxOptions = new FirefoxOptions();
	ChromeOptions chromeOptions = new ChromeOptions();
	
	
	ReadProperties prop = new ReadProperties();
	public WebDriver driver;
	public driverOptions() {
		
	}
	
	public void intializeDriver() {
		
		getDriver();
		driver.manage().window().maximize();
	}

	
	
	public void getDriver() {
		 String value = prop.driver;
		setOptions(value);		 
		if(value.toLowerCase().equals("chrome")) {
			System.setProperty("webdriver.chrome.driver", ChromeURL);
			driver = new ChromeDriver();
		}
		
		else if(value.toLowerCase().equals("ff")) {
			System.setProperty("webdriver.gecko.driver", FFURL);
			driver = new FirefoxDriver();
		}
		
	}
	
	public void setOptions(String browser) {
		if(browser.toLowerCase().equals("chrome")) {
			if(prop.headless.equals("true")) {
				chromeOptions.setHeadless(true);
			}
		}
		else if(browser.toLowerCase().equals("ff")) {
			if(prop.headless.equals("true")) {
				firefoxOptions.setHeadless(true);
			}
		}
	}
}
