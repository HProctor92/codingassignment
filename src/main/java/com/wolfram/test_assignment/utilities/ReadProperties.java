package com.wolfram.test_assignment.utilities;

import java.io.InputStream;
import java.util.Properties;

public class ReadProperties {

	static Properties prop;
	
	static String driver;
	static String headless;
	static String driverAddArguments;
	static String driverSetLogLevel;
	public static String url;
	
	public ReadProperties() {
		try(InputStream input = getClass().getClassLoader().getResourceAsStream("config.properties")) {
			 prop = new Properties();
			 prop.load(input);
			 System.out.println(prop.toString());
			 
			 driver = prop.getProperty("driver");
			 headless = prop.getProperty("headless");
			 driverAddArguments = prop.getProperty("driverAddArguments");
			 driverSetLogLevel = prop.getProperty("driverSetLogLevel");
			 url = prop.getProperty("url");
			 
		 }
		 
		 catch(Exception ex) {
			 ex.printStackTrace();
			 System.out.println("Test not run");
		 }
	}
}
