package com.wolfram.test_assignment.utilities;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

public class UtilityMethods {
	WebDriver driver;
	Actions builder;
	WebDriverWait wait;
	
	
	public UtilityMethods(WebDriver driver, Actions builder, WebDriverWait wait) {
		this.driver = driver;
		this.builder = builder;
		this.wait = wait;
	}
	
	//method found at https://devqa.io/selenium/webdriver-wait-page-load-example-java/
	public void waitForLoad() {
        ExpectedCondition<Boolean> pageLoadCondition = new
                ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
                    }
                };
        wait.until(pageLoadCondition);
    }
}
