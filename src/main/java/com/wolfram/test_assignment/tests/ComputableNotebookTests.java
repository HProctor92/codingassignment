package com.wolfram.test_assignment.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.wolfram.test_assignment.pages.ComputableNotebookPage;
import com.wolfram.test_assignment.pages.HomePage;
import com.wolfram.test_assignment.pages.ResultsPage;
import com.wolfram.test_assignment.utilities.ReadProperties;
import com.wolfram.test_assignment.utilities.driverOptions;

public class ComputableNotebookTests {

	ReadProperties prop = new ReadProperties();
	WebDriver driver = null;
	Actions builder = null;
	WebDriverWait wait;
	
	ResultsPage rp;
	HomePage hp;
	ComputableNotebookPage notebook;
	
	
	@BeforeTest
	public void beforeTestSuite() {
		driverOptions drOp = new driverOptions();
		drOp.intializeDriver();
		driver = drOp.driver;
		builder = new Actions(driver);
		wait = new WebDriverWait(driver, 30);
		driver.get(prop.url);
	}

	
	@Test
	public void getToPageArithmetic() throws InterruptedException {

		hp = new HomePage(driver, builder, wait);
		hp.search("2+2");
		
		//go to computable notebook from input
		rp = new ResultsPage(driver, builder, wait);
		rp.goToComputableNotebook("input");
		notebook = new ComputableNotebookPage(driver, builder, wait);
		notebook.verifyInput("2+2");
	}
	
	
	@Test
	public void getToPageElement() throws InterruptedException {
		hp = new HomePage(driver, builder, wait);
		hp.search("selenium");
		
		//go to computable notebook from input
		rp = new ResultsPage(driver, builder, wait);
		Thread.sleep(5000);
		rp.goToComputableNotebook("input");
		notebook = new ComputableNotebookPage(driver, builder, wait);
		Thread.sleep(5000);
		notebook.verifyInput("selenium");
	}

	@Test
	public void verifySidebar() throws InterruptedException {
		hp = new HomePage(driver, builder, wait);
		hp.search("2+2");
		
		//go to computable notebook from input
		rp = new ResultsPage(driver, builder, wait);
		rp.goToComputableNotebook("input");
		notebook = new ComputableNotebookPage(driver, builder, wait);
		Thread.sleep(5000);
		notebook.verifySidebar();
	}
	
	@AfterMethod
	public void afterMethod() {
		driver.get(prop.url);
		hp.pageLoad();
		builder = new Actions(driver);
	}
	
	@AfterTest
	public void afterTestSuite() {
		driver.quit();
	}
}
