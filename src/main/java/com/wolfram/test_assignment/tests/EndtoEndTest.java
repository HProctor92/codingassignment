package com.wolfram.test_assignment.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.wolfram.test_assignment.pages.ComputableNotebookPage;
import com.wolfram.test_assignment.pages.HomePage;
import com.wolfram.test_assignment.pages.ResultsPage;
import com.wolfram.test_assignment.utilities.ReadProperties;
import com.wolfram.test_assignment.utilities.driverOptions;

public class EndtoEndTest {

	ReadProperties prop = new ReadProperties();
	WebDriver driver = null;
	Actions builder = null;
	WebDriverWait wait;
	
	
	@BeforeTest
	public void beforeTest() {
		driverOptions drOp = new driverOptions();
		drOp.intializeDriver();
		driver = drOp.driver;
		builder = new Actions(driver);
		wait = new WebDriverWait(driver, 30);
	}


	@Test
	public void test() throws InterruptedException {

		//open page and search for 2+2
		driver.get(prop.url);
		HomePage hp = new HomePage(driver, builder, wait);
		hp.search("2+2");
		
		//go to computable notebook from input
		ResultsPage rp = new ResultsPage(driver, builder, wait);
		rp.goToComputableNotebook("input");
		
		//verify that notebook computers input correctly
		ComputableNotebookPage notebook = new ComputableNotebookPage(driver, builder, wait);
		int output = Integer.parseInt(notebook.runInput());
		Assert.assertEquals(4, output);
		
		String numberName = notebook.runNumberName();
		Assert.assertEquals(numberName, "four");
	}


	@AfterTest
	public void afterTest() {
		driver.quit();
	}
}
