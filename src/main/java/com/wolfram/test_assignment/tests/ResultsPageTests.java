package com.wolfram.test_assignment.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.wolfram.test_assignment.pages.ComputableNotebookPage;
import com.wolfram.test_assignment.pages.HomePage;
import com.wolfram.test_assignment.pages.ResultsPage;
import com.wolfram.test_assignment.utilities.ReadProperties;
import com.wolfram.test_assignment.utilities.driverOptions;

public class ResultsPageTests {

	ReadProperties prop = new ReadProperties();
	WebDriver driver = null;
	Actions builder = null;
	WebDriverWait wait;
	
	ResultsPage rp;
	HomePage hp;
	
	
	@BeforeTest
	public void beforeTestSuite() {
		driverOptions drOp = new driverOptions();
		drOp.intializeDriver();
		driver = drOp.driver;
		builder = new Actions(driver);
		wait = new WebDriverWait(driver, 30);
		driver.get(prop.url);
	}

	
	@Test
	public void arithmeticPage() throws InterruptedException {

		//open page and search for 2+2;
		hp = new HomePage(driver, builder, wait);
		hp.search("2+2");
		
		//verify results page loads correctly
		rp = new ResultsPage(driver, builder, wait);
		rp.verifyInput("2 + 2");
		rp.verifySections("arithmetic");
		rp.verifyHoverOptions("arithmetic");
	}
	
	
	@Test
	public void elementPage() throws InterruptedException {
		//open page and search for selenium
		hp = new HomePage(driver, builder, wait);
		hp.search("selenium");
		
		//get chemical element result
		rp = new ResultsPage(driver, builder, wait);
		rp.verifyInput("selenium (chemical element)");
		rp.verifySections("element");
		rp.verifyHoverOptions("element");
	}

	@AfterMethod
	public void afterMethod() {
		rp.goHome();
		hp.pageLoad();
	}
	
	@AfterTest
	public void afterTestSuite() {
		driver.quit();
	}
}
