package com.wolfram.test_assignment.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.wolfram.test_assignment.utilities.UtilityMethods;

public class ResultsPage {

	WebDriver driver;
	Actions builder;
	WebDriverWait wait;

	public ResultsPage(WebDriver driver, Actions builder, WebDriverWait wait) throws InterruptedException {
		this.driver = driver;
		this.builder = builder;
		this.wait = wait;
		Thread.sleep(3000);
	}

	// Elements on page
	By homeLogo = By.xpath("//img[@alt='WolframAlpha computational knowledge AI']");

	// Sections
	By inputSection = By.xpath("//h2[contains(text(), 'Input')]/../..");
	String inputText = "//img[@alt=";
	By resultSection = By.xpath("//h2[contains(text(), 'Result')]/../..");
	By numberLineSection = By.xpath("//h2[contains(text(), 'Number line')]/../..");
	By numberNameSection = By.xpath("//h2[contains(text(), 'Number name')]/../..");
	By manipulativesIllustrationSection = By.xpath("//h2[contains(text(), 'Manipulatives illustration')]/../..");
	By humanComputationTimeSection = By.xpath("//h2[contains(text(), 'Typical human computation times')]/../..");
	By periodicTableLocationSection = By.xpath("//h2[contains(text(), 'Periodic table location')]/../..");
	By imageSection = By.xpath("//h2[contains(text(), 'Image')]/../..");
	By basicElementalPropertiesSection = By.xpath("//h2[contains(text(), 'Basic elemental properties')]/../..");
	By thermodynamicPropertiesSection = By.xpath("//h2[contains(text(), 'Thermodynamic properties')]/../..");
	By materialPropertiesSection = By.xpath("//h2[contains(text(), 'Material properties')]/../..");
	By electromagneticPropertiesSection = By.xpath("//h2[contains(text(), 'Electromagnetic properties')]/../..");
	By reactivitySection = By.xpath("//h2[contains(text(), 'Reactivity')]/../..");
	By atomicPropertiesSection = By.xpath("//h2[contains(text(), 'Atomic properties')]/../..");
	By abundancesSection = By.xpath("//h2[contains(text(), 'Abundances')]/../..");
	By nuclearPropertiesSection = By.xpath("//h2[contains(text(), 'Nuclear properties')]/../..");
	By identifiersSection = By.xpath("//h2[contains(text(), 'Identifiers')]/../..");
	By queryError = By.xpath("//span[text() = \"Wolfram|Alpha doesn't understand your query\"]");

	// hover options
	By enlargeLink = By.xpath("//button/span/span[contains(text(), 'Enlarge')]");
	By dataLink = By.xpath("//button/span/span[contains(text(), 'Data')]");
	By customizeLink = By.xpath("//button/span/span[contains(text(), 'Customize')]");
	By plainTextLink = By.xpath("//button/span/span[contains(text(), 'Plain Text')]");
	By computableNotebook = By.xpath("//button/span/span[contains(text(), 'computable notebook')]");

	// methods for page

	// go to the computable notbook page
	public void goToComputableNotebook(String section) {
		if (section.toLowerCase().equals("input")) {
			wait.until(ExpectedConditions.visibilityOfElementLocated(inputSection));
			builder.moveToElement(driver.findElement(inputSection));
			builder.build().perform();
		}
		wait.until(ExpectedConditions.visibilityOfElementLocated(plainTextLink));
		driver.findElement(plainTextLink).click();
		driver.findElement(computableNotebook).click();
	}

	// go to the home page
	public void goHome() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(homeLogo));
		driver.findElement(homeLogo).click();
	}

	// verify input
	public void verifyInput(String input) {
		// Input must have spaces between numbers and operators
		inputText = inputText + "'" + input + "']";
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(inputText)));
	}

	// verify correct sections are displayed
	public void verifySections(String inputType) {
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(inputSection));

			switch (inputType.toLowerCase()) {

			case "arithmetic":
				wait.until(ExpectedConditions.visibilityOfElementLocated(resultSection));
				wait.until(ExpectedConditions.visibilityOfElementLocated(numberLineSection));
				wait.until(ExpectedConditions.visibilityOfElementLocated(numberNameSection));
				wait.until(ExpectedConditions.visibilityOfElementLocated(manipulativesIllustrationSection));
				wait.until(ExpectedConditions.visibilityOfElementLocated(humanComputationTimeSection));
				break;

			case "element":
				wait.until(ExpectedConditions.visibilityOfElementLocated(periodicTableLocationSection));
				wait.until(ExpectedConditions.visibilityOfElementLocated(imageSection));
				wait.until(ExpectedConditions.visibilityOfElementLocated(basicElementalPropertiesSection));
				wait.until(ExpectedConditions.visibilityOfElementLocated(thermodynamicPropertiesSection));
				wait.until(ExpectedConditions.visibilityOfElementLocated(materialPropertiesSection));
				wait.until(ExpectedConditions.visibilityOfElementLocated(electromagneticPropertiesSection));
				wait.until(ExpectedConditions.visibilityOfElementLocated(reactivitySection));
				wait.until(ExpectedConditions.visibilityOfElementLocated(atomicPropertiesSection));
				wait.until(ExpectedConditions.visibilityOfElementLocated(abundancesSection));
				wait.until(ExpectedConditions.visibilityOfElementLocated(nuclearPropertiesSection));
				wait.until(ExpectedConditions.visibilityOfElementLocated(identifiersSection));
			}
		}

		catch (Exception ex) {
			Assert.fail("Not all sections loaded for results");
		}
	}

	// verify options appear when section is hovered over
	public void verifyHoverOptions(String inputType) {
		try {

			wait.until(ExpectedConditions.visibilityOfElementLocated(inputSection));
			builder.moveToElement(driver.findElement(inputSection));
			builder.build().perform();
			switch (inputType.toLowerCase()) {

			case "arithmetic":
				wait.until(ExpectedConditions.visibilityOfElementLocated(enlargeLink));
				wait.until(ExpectedConditions.visibilityOfElementLocated(dataLink));
				wait.until(ExpectedConditions.visibilityOfElementLocated(customizeLink));
				wait.until(ExpectedConditions.visibilityOfElementLocated(plainTextLink));
				break;

			case "element":
				wait.until(ExpectedConditions.visibilityOfElementLocated(enlargeLink));
				wait.until(ExpectedConditions.visibilityOfElementLocated(customizeLink));
				wait.until(ExpectedConditions.visibilityOfElementLocated(plainTextLink));
				break;
			}
		}

		catch (Exception ex) {
			Assert.fail("Not all options loaded as expected");
		}
	}

	// verify error
	public void verifyError(String input) {
		// input must be an error
		try {
			wait = new WebDriverWait(driver, 10);
			inputText = inputText + "'" + input + "']";
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(inputText)));
		}

		catch (Exception ex) {

		}

		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(queryError));
	}
}
