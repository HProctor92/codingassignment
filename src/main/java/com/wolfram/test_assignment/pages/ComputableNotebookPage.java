package com.wolfram.test_assignment.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.wolfram.test_assignment.utilities.UtilityMethods;

public class ComputableNotebookPage {

	WebDriver driver;
	Actions builder;
	WebDriverWait wait;
		
		public ComputableNotebookPage(WebDriver driver, Actions builder, WebDriverWait wait) throws InterruptedException {
			this.driver = driver;
			this.builder = builder;
			this.wait = wait;
			Thread.sleep(7000);
		}
		
		//Elements on page
		String WAInputString = "//div[@class='SubLayoutBox-Inner PaneSelectorBox-Inner border-box-sizing']/div/div/div/div/div/span[contains(@data-token-text, 'replace')]";
		By inputRunButton = By.xpath("//span[text() = 'Input:']/../../../../td[2]/div");
		By inputOutput = By.xpath("//div[@class='notebook']/div[9]/div/div[1]/div/div/div/div/span");
		By numberNameRunButton = By.xpath("//div[@class='notebook']/div[10]/descendant::div[@class='SubLayoutBox-Inner FrameBox-Inner border-box-sizing']");
		By numberNameOutput = By.xpath("//div[@class='notebook']/div[12]/div/div[1]/div/div/div/div/span[2]");
		
		//Sidebar elements
		By introForStudentsLink = By.xpath("//a[@href = 'http://www.wolfram.com/language/fast-introduction-for-math-students']");
		By subscribeMathematicaLink = By.xpath("//a[@href = 'http://www.wolfram.com/mathematica/online']");
		By introToWolframBookLink = By.xpath("//a[@href = 'https://www.wolfram.com/language/elementary-introduction']");
		By introForProgrammersLink = By.xpath("//a[@href = 'https://www.wolfram.com/language/fast-introduction-for-programmers']");
		By learmMoreLink = By.xpath("//a[@href = 'http://www.wolfram.com/language']");
		
		//methods on page
		
		public String runInput() {
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(inputRunButton));
			driver.findElement(inputRunButton).click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(inputOutput));
			return driver.findElement(inputOutput).getText();
		}
		
		public void verifyInput(String inputExpected) {
			WAInputString = WAInputString.replace("replace", inputExpected);
			By WAInput = By.xpath(WAInputString);
			String inputActual = driver.findElement(WAInput).getAttribute("data-token-text");
			Assert.assertTrue(inputActual.contains(inputExpected));
		}
		
		public String runNumberName() {
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(numberNameRunButton));
			driver.findElement(numberNameRunButton).click();
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(numberNameOutput));
			return driver.findElement(numberNameOutput).getText();
		}
		
		public void verifySidebar() {
			//try {
				wait.until(ExpectedConditions.visibilityOfElementLocated(introForStudentsLink));
				wait.until(ExpectedConditions.visibilityOfElementLocated(subscribeMathematicaLink));
				wait.until(ExpectedConditions.visibilityOfElementLocated(introToWolframBookLink));
				wait.until(ExpectedConditions.visibilityOfElementLocated(introForProgrammersLink));
				wait.until(ExpectedConditions.visibilityOfElementLocated(learmMoreLink));
		//	}
//			catch(Exception ex) {
//				Assert.fail("Sidebar is not loaded correctly");
//			}
		}
		
}
