package com.wolfram.test_assignment.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wolfram.test_assignment.utilities.UtilityMethods;

public class HomePage {

	WebDriver driver;
	Actions builder;
	WebDriverWait wait;
	
	UtilityMethods utility;
	
	public HomePage(WebDriver driver, Actions builder, WebDriverWait wait) {
		this.driver = driver;
		this.builder = builder;
		this.wait = wait;
		utility = new UtilityMethods(driver, builder, wait);
		utility.waitForLoad();
	}
	
	
	//Element on page
	By searchBar = By.xpath("//input");
	By submitButton = By.xpath("//button[@type='submit']");
	By Mathematics = By.xpath("//a[@aria-label='Mathematics']");
	
	
	//methods on page
	public void search(String searchString) {
		driver.findElement(searchBar).click();
		driver.findElement(searchBar).sendKeys(searchString);
		driver.findElement(submitButton).click();
	}
	
	public void pageLoad() {
		utility.waitForLoad();
		wait.until(ExpectedConditions.elementToBeClickable(Mathematics));
	}
}
